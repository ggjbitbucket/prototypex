﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ConnectivityManager : MonoBehaviour
{
	private const string PING_DESTINATION_URL = "http://www.tripixstudio.com/DoNotDelete.html";
	private const float PING_TIME_INTERVAL = 3f;
	private const int FAILED_ATTEMPTS_THRESHOLD = 3;
	private int failedAttempts;
	private bool hasPingedDestination;
	private static bool hasInternet = false;

	public static bool HasInternet
	{
		get {
			return hasInternet;
		}
	}

	void Awake ()
	{
		DontDestroyOnLoad (this);
		StartCoroutine (CheckConnectivityCoroutine ());
	}

	private IEnumerator CheckConnectivityCoroutine ()
	{
		bool isNetworkReachable = false;

		while (true)
		{
			isNetworkReachable = Application.internetReachability != NetworkReachability.NotReachable;
			if (isNetworkReachable)
			{
				yield return StartCoroutine (PingDestinationCoroutine ());

				if(hasInternet != hasPingedDestination)
				{
					if (!hasPingedDestination)
					{
						failedAttempts ++;
						if (failedAttempts > FAILED_ATTEMPTS_THRESHOLD)
						{
							hasInternet = false;

							//TODO : Throw notification
						}
					} 
					else
					{
						failedAttempts = 0;
						hasInternet = true;
						//TODO : Throw notification
					}
				}
			} 
			else
			{
				if(hasInternet)
				{
					hasInternet = false;
					//TODO : Throw notification
				}
			}

			yield return new WaitForSeconds(PING_TIME_INTERVAL);
		}
	}

	private IEnumerator PingDestinationCoroutine ()
	{
		hasPingedDestination = false;
		WWW www = new WWW (PING_DESTINATION_URL);
		yield return www;
		
		if (www.error == null)
		{
			hasPingedDestination = true;
		}
	}
}
