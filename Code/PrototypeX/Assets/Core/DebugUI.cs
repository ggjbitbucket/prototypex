﻿using UnityEngine;
using System.Collections;

public delegate void DebugUIDelegate ();

public class DebugUI : MonoBehaviour
{
	public static DebugUIDelegate onDebugView;
	public int buttonWidth;
	public int buttonHeight;
	
	public static int ButtonWidth
	{
		get {
			return Instance.buttonWidth;
		}
	}
	
	public static int ButtonHeight
	{
		get {
			return Instance.buttonHeight;
		}
	}
	
	private static DebugUI  instance;

	private static DebugUI  Instance
	{
		get {
			if (instance == null)
			{
				instance = FindObjectOfType (typeof(DebugUI)) as DebugUI;
			}
			return instance;
		}
	}

	private bool _showGUI;
	
	void Awake ()
	{
		if (Screen.width >= 2048f)
		{
			buttonWidth *= 2;
			buttonHeight *= 2;
		}
		
		instance = this;
	}
	
	#if TDEBUG || UNITY_EDITOR
	void OnGUI()
	{
		if(GUILayout.Button("Show GUI", GUILayout.Width(buttonWidth), GUILayout.Height(buttonHeight)))
		{
			_showGUI = !_showGUI;
		}
		
		if(_showGUI)
		{
			if(GUILayout.Button("Clear PlayerPrefs", GUILayout.Width(buttonWidth), GUILayout.Height(buttonHeight)))
			{
				PlayerPrefs.DeleteAll();
			}
			
			onDebugView();
		}
	}
	#endif
}
