using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FSM : MonoBehaviour
{
	private Dictionary<string, FSMState> stateMap;
	private FSMState currentState;
	private bool isRunning;
	
	void Awake ()
	{
		stateMap = new Dictionary<string, FSMState> ();
		FSMState[] states = GetComponents<FSMState> ();
		
		for (int i = 0; i < states.Length; i++) 
		{
			AddState(states[i]);
		}
	}
	
	private void AddState(FSMState state)
	{
		TDebug.Assert(!stateMap.ContainsKey(state.StateId), "Cannot add the same state more than once.");
		stateMap.Add(state.StateId, state);
		state.fsm = this;
	}
	
	public void PerformTransition (string transitionId)
	{
		string stateId = currentState.GetOutputState(transitionId);
		TDebug.Assert(stateMap.ContainsKey(stateId), string.Format("No state with ID {0} can be found.", stateId));
		TDebug.Assert(isRunning, "Can only perform state transition whilst the FSM is running");
		
		currentState.OnExit();
		currentState = stateMap[stateId];
		currentState.OnEnter();
	}
	
	public void StartFSM(string stateId)
	{
		TDebug.Assert(stateMap.ContainsKey(stateId), string.Format("No state with ID {0} can be found.", stateId));
		
		if(!isRunning)
		{
			currentState = stateMap[stateId];
			currentState.OnEnter();
			isRunning = true;
		}
	}
	
	public void StopFSM()
	{
		if(isRunning)
		{
			currentState.OnExit();
			isRunning = false;
		}
	}
	
	void Update()
	{
		if(isRunning)
		{
			currentState.OnUpdate();
		}
	}
}
