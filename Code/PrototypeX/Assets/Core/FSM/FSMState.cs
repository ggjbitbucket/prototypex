using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class FSMState : MonoBehaviour {
	
	[HideInInspector]
	public FSM fsm;
	
	public abstract string StateId { get; }
	
	public FSMTransition[] transitions;
	private Dictionary<string, string> transitionMap;
	
	public abstract void OnEnter();
	public abstract void OnUpdate();
	public abstract void OnExit();
	
	void Awake()
	{
		transitionMap = new Dictionary<string, string>();
		
		for(int i = 0; i < transitions.Length; i++)
		{
			AddTransition(transitions[i].transitionId, transitions[i].stateId);
		}
	}
	
	public virtual void AddTransition(string transitionId, string stateId)
	{
		TDebug.Assert(!transitionMap.ContainsKey(transitionId), "Cannot add the same transition more than once.");
		transitionMap.Add(transitionId, stateId);
	}
	
	public virtual string GetOutputState(string transitionId)
	{
		TDebug.Assert(transitionMap.ContainsKey(transitionId), string.Format("No transition with ID {0} can be found.", transitionId));
		return transitionMap[transitionId];
	}
}
