using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class FSMTransition {

	public string transitionId;
	public string stateId;
}
