﻿using UnityEngine;
using System.Collections;

public class GUIHandlerScript : MonoBehaviour {

	public GameObject player1Object;
	public GameObject player2Object;

	//PlayerController p1Controller;
	//PlayerController p2Controller;

	float timer;

	// Use this for initialization
	void Start () {
	
		//p1Controller = player1Object.GetComponent<PlayerController>();
		//p2Controller = player2Object.GetComponent<PlayerController>();

	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
	}

	//OnGUI
	void OnGUI()
	{
		int minutes = Mathf.FloorToInt(timer / 60F);
		int seconds = Mathf.FloorToInt(timer - minutes * 60);
		
		string niceTime = string.Format("{0:00}:{1:00}", minutes, seconds);
		
		GUI.Label(new Rect(10,10,250,100), niceTime);
	}

}
